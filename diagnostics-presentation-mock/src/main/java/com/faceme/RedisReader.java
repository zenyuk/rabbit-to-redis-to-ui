package com.faceme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.inject.Inject;

@Component
@EnableScheduling
public class RedisReader {

    private final static Logger LOG = LoggerFactory.getLogger(RedisReader.class);

    @Inject
    private RedisTemplate redisTemplate;

    @Value("${redis.key.format}")
    private String redisKeyFormat;

    @Scheduled(fixedDelay = 1000)
    public void read() {
        String key = String.format(redisKeyFormat, 9382, 30812);
        ListOperations<String, RtcStats> ops = redisTemplate.opsForList();
        RtcStats result = ops.leftPop(key);

        if (result != null && result.getVideo() != null)
            LOG.info("from Redis: codec - {}, source - {}", result.getVideo().getCodec(), result.getSource());
    }

    @Scheduled(fixedDelay = 1000)
    public void readClient() {
        String key = String.format(redisKeyFormat, 9382, 111);
        ListOperations<String, RtcStats> ops = redisTemplate.opsForList();
        RtcStats result = ops.leftPop(key);

        if (result != null && result.getVideo() != null)
            LOG.info("from Redis: codec - {}, source - {}", result.getVideo().getCodec(), result.getSource());
    }
}
