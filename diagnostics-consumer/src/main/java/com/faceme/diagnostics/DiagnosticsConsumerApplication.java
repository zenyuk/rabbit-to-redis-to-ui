package com.faceme.diagnostics;

import com.faceme.RtcStats;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.AbstractMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@SpringBootApplication
public class DiagnosticsConsumerApplication {

    // RabbitMQ

    @Bean
    ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("localhost");
    }

    @Bean
    AbstractMessageListenerContainer listenerContainer() {
        SimpleMessageListenerContainer listenerContainer = new SimpleMessageListenerContainer(connectionFactory());
        listenerContainer.setQueueNames("myQueue");
        return listenerContainer;
    }

    // Redis

    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
        jedisConFactory.setHostName("localhost");
        jedisConFactory.setPort(6379);
        return jedisConFactory;
    }

    @Bean
    StringRedisSerializer stringRedisSerializer() {
        return new StringRedisSerializer();
    }

    @Bean
    RedisTemplate<String, RtcStats> redisTemplate() {
        RedisTemplate<String, RtcStats> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.setKeySerializer(stringRedisSerializer());
        return template;
    }

    // Ehcache

    @Bean
    CacheManager cacheManager() {
        return CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("preConfigured", CacheConfigurationBuilder.newCacheConfigurationBuilder(
                        String.class,
                        Long.class,
                        ResourcePoolsBuilder.heap(100)
                        ).build()
                ).build(true);
    }

    @Bean
    Cache<String, Long> meetingSessionIdCache() {
        return cacheManager().createCache(
                "meetingSessionIdCache",
                CacheConfigurationBuilder.newCacheConfigurationBuilder(
                        String.class,
                        Long.class,
                        ResourcePoolsBuilder.heap(100)
                ).build());
    }

    @Bean
    Cache<String, Long> lobbyPersonIdCache() {
        return cacheManager().createCache(
                "lobbyPersonIdCache",
                CacheConfigurationBuilder.newCacheConfigurationBuilder(
                        String.class,
                        Long.class,
                        ResourcePoolsBuilder.heap(100)
                ).build());
    }

    public static void main(String[] args) {
        SpringApplication.run(DiagnosticsConsumerApplication.class, args);
    }
}
