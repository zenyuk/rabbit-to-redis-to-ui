package com.faceme.diagnostics;

import com.faceme.RtcStatsInputContract;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.listener.AbstractMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;

@Component
public class Consumer {

    @Inject
    private ObjectMapper objectMapper;

    @Inject
    AbstractMessageListenerContainer listenerContainer;

    @Inject
    Broker broker;

    private final static Logger LOG = LoggerFactory.getLogger(Consumer.class);

    @PostConstruct
    public void init() {
        listenerContainer.setMessageListener(new MessageListenerAdapter(this));
        listenerContainer.start();
    }

    /**
     * Consume RabbitMQ messages and map to the Bean
     */
    // TODO: 2/8/17 try to use RtcStatsInputContract instead fo String
    public void handleMessage(String serializedMsgFromAmqp) throws IOException {

        RtcStatsInputContract stats = null;

        try {
            stats = objectMapper.readValue(serializedMsgFromAmqp, RtcStatsInputContract.class);
        } catch (JsonProcessingException ex) {
            // ignore not properly formatted messages
            LOG.error("Can't parse message: %s", serializedMsgFromAmqp);
            return;
        }

        broker.processMessage(stats);
    }


}
