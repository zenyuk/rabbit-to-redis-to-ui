package com.faceme;

import lombok.Data;

import java.io.Serializable;

public @Data
class MediaStats implements Serializable{

    static final long serialVersionUID = 1L;

    private Long recvLost;
    private Long recvTotal;
    private Long sendLost;
    private Long sendTotal;
    private String codec;
}