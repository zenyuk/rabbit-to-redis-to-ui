package com.faceme.diagnostics;

import com.faceme.MediaStats;
import com.faceme.RtcStatsInputContract;
import com.faceme.RtcStatsSource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
@EnableScheduling
public class Emitter {

    private final static Logger LOG = LoggerFactory.getLogger(Emitter.class);

    @Inject
    RabbitTemplate rabbitTemplate;

    @Inject
    private ObjectMapper objectMapper;

    @Scheduled(fixedDelay = 3_000)
    public void emit() throws JsonProcessingException {
        LOG.info("============ emit from vcgw ==================");

        MediaStats videoStats = new MediaStats();
        videoStats.setCodec("VP8");
        videoStats.setSendTotal(1254l);

        RtcStatsInputContract stats = new RtcStatsInputContract();
        stats.setTimestamp("1234567890");
        stats.setRtt(1l);
        stats.setJitter(1l);
        stats.setVcInstance(2);
        stats.setStreamName("zyhmdkdknumlrjgwrguf");
        stats.setVideo(videoStats);
        stats.setSource(RtcStatsSource.VCGW);

        String msg = objectMapper.writeValueAsString(stats);

        rabbitTemplate.convertAndSend(msg);
    }
}
