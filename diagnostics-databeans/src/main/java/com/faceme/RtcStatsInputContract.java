package com.faceme;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RtcStatsInputContract implements Serializable {
    // don't change to keep backward compatibility
    static final long serialVersionUID = 1L;

    private String timestamp;
    private Long rtt;
    private Long jitter;
    private Integer vcInstance;
    private String streamName;
    private RtcStatsSource source;
    private MediaStats video;
    private MediaStats audio;
}