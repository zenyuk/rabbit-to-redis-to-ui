package com.faceme.diagnostics;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.regex.Pattern;

@Component
public class SqlRepository {

    @Inject
    JdbcTemplate jdbcTemplate;

    private final String nonAlphanumericRegex = ".*\\W.*";

    public Long getMeetingSessionIdByStreamName(String streamName) {

        if (Pattern.matches(nonAlphanumericRegex, streamName))
            throw new IllegalArgumentException("SqlRepository::getMeetingSessionIdByStreamName " + streamName);

        String sql = "SELECT session_id FROM Attendee AS a JOIN LobbyPerson AS l ON a.lobby_person_id = l.id WHERE l.stream_name = ?";
        return jdbcTemplate.queryForObject(sql, Long.class, new Object[] { streamName });
    }

    public Long getLobbyPersonIdByStreamName(String streamName) {

        if (Pattern.matches(nonAlphanumericRegex, streamName))
            throw new IllegalArgumentException("SqlRepository::getLobbyPersonIdByStreamName " + streamName);

        String sql = "SELECT id FROM LobbyPerson WHERE stream_name = ?";
        return jdbcTemplate.queryForObject(sql, Long.class, new Object[] { streamName });
    }
}
