package com.faceme.diagnostics;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DiagnosticsClientsMockApplication {

    @Bean
    ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("localhost");
    }

    @Bean
    RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate();
        rabbitTemplate.setConnectionFactory(connectionFactory());
        rabbitTemplate.setExchange("myExchange");
        rabbitTemplate.setQueue("myQueue");
        rabbitTemplate.setRoutingKey("foo.bar");
        return rabbitTemplate;
    }


    public static void main(String[] args) { SpringApplication.run(DiagnosticsClientsMockApplication.class, args); }
}
