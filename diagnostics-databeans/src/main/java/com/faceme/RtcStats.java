package com.faceme;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RtcStats implements Serializable {
    static final long serialVersionUID = 1L;

    public RtcStats() {
    }

    public RtcStats(RtcStatsInputContract inputContract) {
        this.timestamp = inputContract.getTimestamp();
        this.rtt = inputContract.getRtt();
        this.jitter = inputContract.getJitter();
        this.vcInstance = inputContract.getVcInstance();
        this.streamName = inputContract.getStreamName();
        this.source = inputContract.getSource();
        this.video = inputContract.getVideo();
        this.audio = inputContract.getAudio();
    }

    private String timestamp;
    private Long rtt;
    private Long jitter;
    private Integer vcInstance;
    private String streamName;
    private RtcStatsSource source;
    private MediaStats video;
    private MediaStats audio;

    private Long meetingSessionId;
    private Long lobbyPersonId;
}