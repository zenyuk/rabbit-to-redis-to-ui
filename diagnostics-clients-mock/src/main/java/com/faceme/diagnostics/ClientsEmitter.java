package com.faceme.diagnostics;

import com.faceme.MediaStats;
import com.faceme.RtcStats;
import com.faceme.RtcStatsSource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
@EnableScheduling
public class ClientsEmitter {

    private final static Logger LOG = LoggerFactory.getLogger(ClientsEmitter.class);

    @Inject
    RabbitTemplate rabbitTemplate;

    @Inject
    private ObjectMapper objectMapper;

    @Scheduled(fixedDelay = 5_000)
    public void emit() throws JsonProcessingException {
        LOG.info(">>>>>>>>>>>>> emit from client >>>>>>>>>>>>>>>>>>");

        MediaStats videoStats = new MediaStats();
        videoStats.setCodec("VP8");
        videoStats.setSendTotal(1214l);

        RtcStats stats = new RtcStats();
        stats.setTimestamp("1237777897");
        stats.setRtt(2l);
        stats.setJitter(1l);
        stats.setVideo(videoStats);
        stats.setSource(RtcStatsSource.CHROME);
        stats.setMeetingSessionId(9382l);
        stats.setLobbyPersonId(111l);

        String msg = objectMapper.writeValueAsString(stats);

        rabbitTemplate.convertAndSend(msg);
        rabbitTemplate.convertAndSend(msg);
    }
}
