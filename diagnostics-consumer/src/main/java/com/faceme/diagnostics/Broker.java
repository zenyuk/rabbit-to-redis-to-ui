package com.faceme.diagnostics;

import com.faceme.RtcStats;
import com.faceme.RtcStatsInputContract;
import com.faceme.RtcStatsSource;
import org.ehcache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.sql.SQLException;

@Component
public class Broker {

    @Inject
    private SqlRepository sqlRepository;

    @Inject
    Persistence persistence;

    @Inject
    Cache<String, Long> meetingSessionIdCache;

    @Inject
    Cache<String, Long> lobbyPersonIdCache;

    private final static Logger LOG = LoggerFactory.getLogger(Broker.class);

    /**
     * Process queue messages.
     * Receive, enrich with using MySql state lookup and persist it in Redis
     */
    public void processMessage(RtcStatsInputContract inputMessage) {

        RtcStats resultMessage = new RtcStats(inputMessage);

        // message enriching is fault tolerant
        if (resultMessage.getSource() == RtcStatsSource.VCGW) {
            resultMessage = enrichVcgwMessage(resultMessage);
        } else if (resultMessage.getSource() == RtcStatsSource.CHROME) {
            resultMessage = enrichChromeMessage(resultMessage);
        }

        // persist in Redis
        persistence.save(resultMessage);
    }

    /**
     * Lookup into sql state db for the meeting session detailed info
     */
    RtcStats enrichVcgwMessage(RtcStats message) {

        String streamName = message.getStreamName();

        // try in cache first
        Long meetingSessionId = meetingSessionIdCache.get(streamName);
        if (meetingSessionId == null) {
            try {
                // not cached, get from DB
                meetingSessionId = sqlRepository.getMeetingSessionIdByStreamName(streamName);
            } catch (EmptyResultDataAccessException e) {
                // it's broken message; raise and don't put in processing queue
                LOG.error("Meeting session for stream name = %s not found", streamName);
                throw e;
            }


            // update cache
            meetingSessionIdCache.put(streamName, meetingSessionId);
        }

        Long lobbyPersonId = lobbyPersonIdCache.get(streamName);
        if (lobbyPersonId == null) {
            try {
                // not cached, get from DB
                lobbyPersonId = sqlRepository.getLobbyPersonIdByStreamName(streamName);
            } catch (EmptyResultDataAccessException e) {
                // it's broken message; raise and don't put in processing queue
                LOG.error("Lobby person for stream name = %s not found", streamName);
                throw e;
            }

            // update cache
            lobbyPersonIdCache.put(streamName, lobbyPersonId);
        }

        message.setMeetingSessionId(meetingSessionId);
        message.setLobbyPersonId(lobbyPersonId);

        return message;
    }

    /**
     * Lookup into sql state db for the meeting session detailed info
     */
    RtcStats enrichChromeMessage(RtcStats message) {

        // TODO: 2/8/17 implement

        return message;
    }
}
