package com.faceme.diagnostics;

import com.faceme.RtcStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class Persistence {

    private final static Logger LOG = LoggerFactory.getLogger(Persistence.class);

    @Inject
    RedisTemplate<String, RtcStats> redisTemplate;

    @Value("${redis.key.format}")
    private String redisKeyFormat;

    /**
     * Persist into im-memory database - Redis
     */
    public void save(RtcStats stats) {
        String key = String.format(redisKeyFormat, stats.getMeetingSessionId(), stats.getLobbyPersonId());

        redisTemplate
                .opsForList()
                .leftPush(key, stats);

        LOG.debug("key stored in redis: %s", key);
    }
}
